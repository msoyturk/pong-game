(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)',
            cursor: 'none'
        },
        scoreboard: {
            width: 900,
            height: 100,
            position: 'fixed',
            background: 'rgb(198, 166, 47)', 
            top: '10%',
            left: '50%',
            zIndex: '9999',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            justifyContent: 'space-around',
            cursor: 'none'
        },
        saveButton: {
            width: 900,
            height: 100,
            position: 'fixed',
            background: 'rgb(198, 166, 47)', 
            top: '90%',
            left: '50%',
            zIndex: '9999',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: '5vh'
        },
        openingScreen: {
            width: 900,
            height: 600,
            background: 'green',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            flexWrap: 'wrap',
            color: "red",
            justifyContent : 'space-around',
            alignItems: 'center',
            fontSize: '10vh'
        },
        openingButtons: {
            width: 200,
            height: 100,
            background: 'rgb(198, 166, 47)', 
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: '2vh'
        },
        winnerScreen: {
            width: 900,
            height: 600,
            background: 'green',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)',
            display: 'none',
            color: "red",
            justifyContent : 'center',
            alignItems: 'center',
        },
        player: {
            height: 100,
            width: 300,
            display: 'flex',
            justifyContent : 'center',
            fontSize: '10vh',
            alignItems: 'center',
            color: 'rgb(98, 36, 123)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 150
        },
        stick2: {
            left: 900-12,
            top: 150
        }
    };

    var CONSTS = {
    	gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0,
        gameType: "One-o-One"
    };

    function openFirstScreen() {
        // Game type: Teke tek, bilgisayara karşı, iki pc birbirine karşı
        $('<div/>', {id: 'opening-screen'}).css(CSS.openingScreen).appendTo('body');
        $('<button/>', {id: 'game-classic'}).css(CSS.openingButtons).appendTo('#opening-screen');
        $('#game-classic').html("One-o-One").click(openSelectedGameType);
        $('<button/>', {id: 'game-onecpu'}).css(CSS.openingButtons).appendTo('#opening-screen');
        $('#game-onecpu').html("Against CPU").click(openSelectedGameType);
        $('<button/>', {id: 'game-twocpu'}).css(CSS.openingButtons).appendTo('#opening-screen');
        $('#game-twocpu').html("Two CPU").click(openSelectedGameType);
        $('<button/>', {id: 'saved-game'}).css(CSS.openingButtons).appendTo('#opening-screen');
        $('#saved-game').html("Open Saved Game").click(openSavedGame);
    }

    function openSelectedGameType() {
        $("#opening-screen").remove();
        CONSTS.gameType = $(this).html();
        startGame(true, CONSTS.gameType);
    }

    function openSavedGame() {
        $("#opening-screen").remove();
        setSavedData();
        startGame(false, CONSTS.gameType);
    }

    function startGame(newGame, gameType) {
        if(newGame) {
            saveInitialData();
        }else {
            setSavedData();
        }
        
        draw();
        roll();
        loop(gameType);
    }

    function restartGame() {
        clearGame();
        openFirstScreen();
    }

    function clearGame() {
        $('#pong-game').remove();
        $('#scoreboard').remove();
        $('#newgame-button').remove();
        $('#winner-screen').remove();
        setInitialData();
    }

    function draw() {
        initScoreboard();
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        initButtons();
        $('<div/>', {id: 'winner-screen'}).css(CSS.winnerScreen).appendTo('body');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick))
        .appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick))
        .appendTo('#pong-game');
    }

    function initScoreboard() {
        $('<div/>', {id: 'scoreboard'}).css(CSS.scoreboard).appendTo('body');
        $('<div/>', {id: 'player1'}).css(CSS.player).appendTo('#scoreboard');
        $('<div/>', {id: 'player2'}).css(CSS.player).appendTo('#scoreboard');
        $('#player1').html(CONSTS.score1);
        $('#player2').html(CONSTS.score2);
    }

    function initButtons() {
        $('<button/>', {id: 'save-button'}).css(CSS.saveButton).appendTo('body');
        $('#save-button').html("Save the Game");
        $('#save-button').click(saveDataToStorage);

        $('<button/>', {id: 'newgame-button'}).css(CSS.saveButton).appendTo('body');
        $('#newgame-button').html("New Game");
        $('#newgame-button').click(restartGame);
        $('#newgame-button').css("display", "none");
    }

    function setEvents(onlyOne) {
        $(document).on('keydown', function (e) {
            switch(e.keyCode) {
                case 87:
                    CONSTS.stick1Speed = -20; break;
                case 83:
                    CONSTS.stick1Speed = 20; break;
                case 40: 
                    if(onlyOne) break; CONSTS.stick2Speed = 20; break;
                case 38:
                    if(onlyOne) break; CONSTS.stick2Speed = -20; break;
            }
        });

        $(document).on('keyup', function (e) {
            switch(e.keyCode) {
                case 87:
                    CONSTS.stick1Speed = 0; break;
                case 83:
                    CONSTS.stick1Speed = 0; break;
                case 40: 
                    if(onlyOne) break; CONSTS.stick2Speed = 0; break;
                case 38:
                    if(onlyOne) break; CONSTS.stick2Speed = 0; break;
            }
        });
    }

    function loop(gameType) {
        if(gameType === "One-o-One") {
            setEvents(false);
        }

        window.pongLoop = setInterval(function () {
            if(gameType === "Two CPU") {
                moveCPUPlayers();
            }else if(gameType === "Against CPU") {
                setEvents(true)
                moveCPUPlayer();
            }
            updateSticks();
            CSS.ball.top += CONSTS.ballTopSpeed;
            CSS.ball.left += CONSTS.ballLeftSpeed;

            checkCollisionTopDown();

            $('#pong-ball').css({top: CSS.ball.top,left: CSS.ball.left});

            checkCollisionLeftRight();
        }, CONSTS.gameSpeed);
    }

    function moveCPUPlayer() {
        var centerOfStick2 = Math.round(CSS.stick2.top + (CSS.stick.height / 2));
        var centerOfBall = Math.round(CSS.ball.top + (CSS.ball.height / 2));

        if(centerOfBall - 20 > centerOfStick2) {
            CONSTS.stick2Speed = +5;    
        }else if(centerOfBall + 20 < centerOfStick2) {
            CONSTS.stick2Speed = -5;
        }
    }
    
    function moveCPUPlayers() {
        var centerOfStick1 = Math.round(CSS.stick1.top + (CSS.stick.height / 2));
        var centerOfStick2 = Math.round(CSS.stick2.top + (CSS.stick.height / 2));
        var centerOfBall = Math.round(CSS.ball.top + (CSS.ball.height / 2));
        if(centerOfBall - 20 > centerOfStick1) {
            CONSTS.stick1Speed = +5;    
        }else if(centerOfBall + 20 < centerOfStick1) {
            CONSTS.stick1Speed = -5;
        }

        if(centerOfBall - 20 > centerOfStick2) {
            CONSTS.stick2Speed = +5;    
        }else if(centerOfBall + 20 < centerOfStick2) {
            CONSTS.stick2Speed = -5;
        }
    }

    function checkCollisionTopDown() {
        if (CSS.ball.top <= 0 || CSS.ball.top >= CSS.arena.height - CSS.ball.height) {
            CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
        }
    }

    function checkCollisionLeftRight() {
        if (CSS.ball.left <= CSS.stick.width) {
            if(CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height) {
                CONSTS.ballLeftSpeed *= -1;
            }else {
                CONSTS.score2 += 1;
                $('#player2').html(CONSTS.score2);
                roll();
            }
        }

        if (CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width) {
            if(CSS.ball.top > CSS.stick2.top && CSS.ball.top < CSS.stick2.top + CSS.stick.height) {
                CONSTS.ballLeftSpeed *= -1;
            }else {
                CONSTS.score1 += 1;
                $('#player1').html(CONSTS.score1);
                roll();
            }
        }

        checkWinner();
    }

    function checkWinner() {
        var player1 = parseInt($('#player1').html());
        var player2 = parseInt($('#player2').html());

        if(player1 == 5) {
            $("#winner-screen").css("display", "flex");
            $("#winner-screen").html("Winner is Player 1");
            $("#pong-game").css("display", "none");
            $("#scoreboard").css("display", "none");
            clearInterval(window.pongLoop);
            $("#save-button").remove();
            $("#newgame-button").css("display", "flex");
            
        }else if(player2 == 5) {
            $("#winner-screen").css("display", "flex");
            $("#winner-screen").html("Winner is Player 2");
            $("#pong-game").css("display", "none");
            // $("#scoreboard").css("display", "none");
            clearInterval(window.pongLoop);
            $("#save-button").remove();
            $("#newgame-button").css("display", "flex");
        }
    }

    function saveInitialData() {
        localStorage.setItem("initCSS", JSON.stringify(CSS));
        localStorage.setItem("initConsts", JSON.stringify(CONSTS));
        localStorage.setItem("firstTime", "false");
    }

    function saveDataToStorage() {
        localStorage.setItem("gameCSS", JSON.stringify(CSS));
        localStorage.setItem("consts", JSON.stringify(CONSTS));
    }

    function setInitialData() {
        CSS = JSON.parse(localStorage.getItem('initCSS'));
        CONSTS = JSON.parse(localStorage.getItem('initConsts'));
    }

    function setSavedData() {
        if(!localStorage.getItem('gameCSS') || !localStorage.getItem('consts')) {
            alert("No saved Games"); 
            return;
        }
        CSS = JSON.parse(localStorage.getItem('gameCSS'));
        CONSTS = JSON.parse(localStorage.getItem('consts'));
    }

    function updateSticks() {
        if(CSS.stick1.top + CONSTS.stick1Speed + CSS.stick.height < CSS.arena.height && CONSTS.stick1Speed > 0) {
            CSS.stick1.top += CONSTS.stick1Speed;
            $('#stick-1').css('top', CSS.stick1.top);
        }else if(CSS.stick1.top + CONSTS.stick1Speed > 0 && CONSTS.stick1Speed < 0) {
            CSS.stick1.top += CONSTS.stick1Speed;
            $('#stick-1').css('top', CSS.stick1.top);
        }

        if(CSS.stick2.top + CONSTS.stick2Speed + CSS.stick.height < CSS.arena.height && CONSTS.stick2Speed > 0) {
            CSS.stick2.top += CONSTS.stick2Speed;
            $('#stick-2').css('top', CSS.stick2.top);
        }else if(CSS.stick2.top + CONSTS.stick2Speed > 0 && CONSTS.stick2Speed < 0) {
            CSS.stick2.top += CONSTS.stick2Speed;
            $('#stick-2').css('top', CSS.stick2.top);
        }
    }

    function roll() {
        CSS.ball.top = CSS.arena.height / 2;
        CSS.ball.left = CSS.arena.width / 2;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballTopSpeed = Math.random() * - 8 - 3;
        CONSTS.ballLeftSpeed = side * (Math.random() * 8 + 3);
    }

    openFirstScreen();
})();